import random

# import boto3
import yaml


class EphemeralVPN:
    def __init__(self, config_file):
        self.read_config(config_file)

        self.providers = []
        for provider in self.config['providers']:
            self.providers.append(self.init_provider(provider))

    def read_config(self, config_file):
        with open(config_file) as f:
            self.config = yaml.load(f.read())

    def init_provider(self, provider):
        locations = self.config['providers'][provider]['locations']

        if provider == 'atlantic':
            pass
        elif provider == 'aws':
            access_key = self.config['providers'][provider]['access_key']
            secret_key = self.config['providers'][provider]['secret_key']

            return EphemeralProviderAWS(access_key, secret_key, locations)
        elif provider == 'digitalocean':
            token = self.config['providers'][provider]['token']

            return EphemeralProviderDigitalOcean(token, locations)
        elif provider == 'google_compute':
            account_json = self.config['providers'][provider]['account_json']
            project = self.config['providers'][provider]['project']

            return EphemeralProviderGoogleCompute(account_json, project)
        elif provider == 'joyent':
            account_name = self.config['providers'][provider]['account_name']
            key_id = self.config['providers'][provider]['key_id']

            return EphemeralProviderJoyent(account_name, key_id)
        elif provider == 'linode':
            pass
        elif provider == 'vultr':
            api_key = self.config['providers'][provider]['api_key']

            return EphemeralProviderVultr(api_key)

    def list_instances(self):
        instances = []
        for provider in self.providers:
            instances.append(provider.list_instances())

        return instances

    def random_instance(self):
        provider = random.choice(self.providers)
        location = random.choice(provider.locations)

        provider.create_instance(location)

    def destroy_instances(self):
        for instance in self.list_instances():
            instance['provider'].suicide(instance['instance_id'])


class EphemeralProviderAtlantic:
    def __init__(self, locations):
        self.locations = locations

    def list_instances(self):
        pass

    def create_instance(self, location):
        pass

    def suicide(self, instance_id):
        pass


class EphemeralProviderAWS:
    def __init__(self, access_key, secret_key, locations):
        self.access_key = access_key
        self.secret_key = secret_key
        self.locations = locations

    def list_instances(self):
        pass

    def create_instance(self, location):
        pass

    def suicide(self, instance_id):
        pass


class EphemeralProviderDigitalOcean:
    def __init__(self, token, locations):
        self.token = token
        self.locations = locations

    def list_instances(self):
        pass

    def create_instance(self, location):
        pass

    def suicide(self, instance_id):
        pass


class EphemeralProviderGoogleCompute:
    def __init__(self, account_json, project, locations):
        self.account_json = account_json
        self.project = project
        self.locations = locations

    def list_instances(self):
        pass

    def create_instance(self, location):
        pass

    def suicide(self, instance_id):
        pass


class EphemeralProviderJoyent:
    def __init__(self, account_name, key_id, locations):
        self.account_name = account_name
        self.key_id = key_id
        self.locations = locations

    def list_instances(self):
        pass

    def create_instance(self, location):
        pass

    def suicide(self, instance_id):
        pass


class EphemeralProviderLinode:
    def __init__(self, locations):
        self.locations = locations

    def list_instances(self):
        pass

    def create_instance(self, location):
        pass

    def suicide(self, instance_id):
        pass


class EphemeralProviderVultr:
    def __init__(self, api_key, locations):
        self.api_key = api_key
        self.locations = locations

    def list_instances(self):
        pass

    def create_instance(self, location):
        pass

    def suicide(self, instance_id):
        pass
