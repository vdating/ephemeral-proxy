import yaml

with open('config.yml') as f:
    config = yaml.load(f.read())

for provider in config['providers']:
    print(provider + ':')

    if config['providers'][provider] is None:
        continue

    if 'locations' in config['providers'][provider]:
        for location in config['providers'][provider]['locations']:
            print(location)
