import base64
import calendar
import json
import hashlib
import hmac
import time
import uuid

import requests

from fabric.api import env, put, run


API_PUBLIC_KEY = 'nope'
API_PRIVATE_KEY = 'nope'


class EphemeralProviderAtlantic:
    def __init__(self, api_public_key, api_private_key, locations):
        self.api_public_key = api_public_key
        self.api_private_key = api_private_key
        self.locations = locations

    def list_instances(self):
        response = self.request({'Action': 'list-instances'})
        items = response['list-instancesresponse']['instancesSet']

        instances = []
        for item in items:
            vm = items[item]
            if vm['vm_description'] != 'ephemeralvpn' and \
               vm['vm_status'] != 'RUNNING':
                continue

            instances.append({
                'instance_id': vm['InstanceId'],
                'location': 'unknown',
                'address': vm['vm_ip_address']})

        return instances

    def create_instance(self, location):
        response = self.request({
            'Action': 'run-instance',
            'servername': 'ephemeralvpn',
            'imageid': 'debian-9.0.0_64bit',
            'planname': 'G2.1GB',
            'vm_location': location,
        })

        instance = response['run-instanceresponse']['instancesSet']['item']

        env.hosts = [instance['ip_address']]
        env.user = instance['username']
        env.password = instance['password']

        env.connection_attempts = 16
        env.timeout = 15

        print(env)

        run('apt-get update')
        run('apt-get install -y openvpn')
        run('mkdir /ephemeral')

        put('vpn/*', '/ephemeral/')

        run('chmod +x /ephemeral/init.sh')
        run('/ephemeral/init.sh fabric')
        run('touch /.dockerenv')
        run('sed -i "s/group nobody/group nogroup/" /ephemeral/server.conf')
        run('/ephemeral/init.sh fabric')

        return instance

    def suicide(self, instance_id):
        return self.request({
            'Action': 'terminate-instance',
            'instanceid': instance_id
        })

    def request(self, params):
        endpoint = 'https://cloudapi.atlantic.net/?'

        epoch = str(int(calendar.timegm(time.gmtime())))
        rndguid = uuid.uuid4().hex

        digest = hmac.new(
            self.api_private_key.encode(),
            msg=(epoch + rndguid).encode(),
            digestmod=hashlib.sha256).digest()

        signature = base64.b64encode(digest).decode()

        p = {
            'Version': '2010-12-30',
            'ACSAccessKeyId': self.api_public_key,
            'Format': 'json',
            'Timestamp': epoch,
            'Rndguid': rndguid,
            'Signature': signature,
        }

        for param in params:
            p[param] = params[param]

        return requests.get(endpoint, params=p).json()


provider = EphemeralProviderAtlantic(API_PUBLIC_KEY, API_PRIVATE_KEY, [])
# instances = provider.list_instances()

instance = provider.create_instance('USCENTRAL1')
print(json.dumps(instance))
