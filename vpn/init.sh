#!/bin/bash

EXT_IF=$(ip route show | grep default | sed 's/.*dev \([a-z0-9]*\).*/\1/' | head -1)

if [ ! -f /.dockerenv ]; then
    sysctl -w net.ipv4.ip_forward=1
    iptables -t nat -A POSTROUTING -o $EXT_IF -j MASQUERADE

    if [ "$1" == "fabric" ]; then
        iptables -A INPUT -i $EXT_IF -p tcp --dport 22 -j ACCEPT
    fi

    iptables -A INPUT -i $EXT_IF -p udp --dport 1194 -j ACCEPT
    iptables -A INPUT -i $EXT_IF -m conntrack --ctstate ESTABLISHED,RELATED
    iptables -A INPUT -i $EXT_IF -j DROP
else
    if [ "$1" == "fabric" ]; then
        openvpn --cd /ephemeral --config /ephemeral/server.conf --daemon
    else
        openvpn --cd /ephemeral --config /ephemeral/server.conf
    fi
fi
